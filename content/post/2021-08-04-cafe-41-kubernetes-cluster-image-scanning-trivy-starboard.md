---
Title: "41. #EveryoneCanContribute cafe: Kubernetes Cluster Image Scanning with Trivy & Starboard"
Date: 2021-08-04
Aliases: []
Tags: ["security","gitlab","trivy","kubernetes","starboard"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Niclas Mietz](https://twitter.com/solidnerd) walks us Aqua Security Starboard, installed into a Civo Cloud k3s cluster. [Philip Welz](https://twitter.com/philip_welz) takes over with Trivy in Estafette. 

Reminder: GitLab Commit Virtual day 2 is today. [Register now!](https://gitlabcommitvirtual2021.com/)

### Recording

Enjoy the session! 🦊  

{{< youtube ubzg4s39u6g >}}

<br>

### Highlights

First, the Starboard Operator will be installed and collecting the cluster image reports in our Civo k2s cluster. You can specifiy the namespaces for the Starboard Operator in the [configuration](https://aquasecurity.github.io/starboard/v0.11.0/operator/configuration/). If left empty, all namespaces are scanned - we defined the `default` namespace.

The next step is to combine this with GitLab CI/CD to see the security reports. [Follow the GitLab documentation](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/) to generate the `CIS_KUBECONFIG` variable as file. You can also define additional parameters for the [CI/CD job](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/#cicd-variables-for-cluster-image-scanning).

The Estafette Vulnerability Scanner runs Trivy in a pod in a given interval and reports similar cluster image vulnerabilities. The installation with the Helm chart and `values.yml` override took longer, and the Grafana dashboard sourcing the Prometheus exporter and `ServiceMonitor` resource needed extra attention. 


### Insights

- Documentation
  - [Starboard](https://github.com/aquasecurity/starboard)
  - [Trivy](https://github.com/aquasecurity/trivy)  
  - [Estafette Vulnerability Scanner](https://github.com/estafette/estafette-vulnerability-scanner)
- [civo-k3s repository](https://gitlab.com/everyonecancontribute/kubernetes/civo-k3s)
- [Twitter thread](https://twitter.com/dnsmichi/status/1422554626700754948)
