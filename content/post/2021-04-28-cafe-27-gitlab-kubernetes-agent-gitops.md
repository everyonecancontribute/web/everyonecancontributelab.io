---
Title: "27. #everyonecancontribute cafe: GitLab Kubernetes Agent and GitOps"
Date: 2021-04-28
Aliases: []
Tags: ["gitlab","kubernetes","agent","gitops"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Philip walks us through the new [GitLab Kubernetes Agent](https://about.gitlab.com/releases/2021/04/22/gitlab-13-11-released/) together with Niclas and Nico. 

### Recording

Enjoy the session! 🦊 

{{< youtube 1T-g5oh_GSg >}}

<br>

### Highlights

- [GitLab Kubernetes Agent Documentation](https://docs.gitlab.com/ee/user/clusters/agent/)
- [3 ways to approach GitOps](https://about.gitlab.com/blog/2021/04/27/gitops-done-3-ways/)
- Generate an auth token for the agent, needs GraphQL 
- Create a local k8s cluster with [kind](https://kind.sigs.k8s.io/docs/user/configuration/): `kind create cluster` or `kind create cluster --config CONFIG.yaml` 
- Create a service account and cluster role bindings 
- Agent pulls changes
- Learn more about [Project Manifest synchronization](https://docs.gitlab.com/ee/user/clusters/agent/repository.html#synchronize-manifest-projects)
- Define the [Podtato head](https://github.com/cncf/podtato-head) demo repository and deploy the app,
- Learned to create multiple repositories - k8s-agent, and the application repository. Otherwise YAML inception and permissions override may happen.

We'll continue in future sessions :)

### Insights

- Kubernetes agent
  - [Direction / Roadmap](https://about.gitlab.com/direction/configure/kubernetes_management/)
  - [Planning Epic](https://gitlab.com/groups/gitlab-org/-/epics/3329)
  - [Repository](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent)
- Demo repositories
  - [GitLab Kubernetes agent](https://gitlab.com/everyonecancontribute/kubernetes/k8s-agent)
  - [Gitpod examples](https://gitlab.com/everyonecancontribute/kubernetes/gitpod-examples)
  - [GitLab GitOps Demo](https://gitlab.com/gitlab-examples/ops/configure-product-walkthrough/)
- [Miro board](https://miro.com/app/board/o9J_lTOCVrg=/)
- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)
- [Twitter](https://twitter.com/dnsmichi/status/1387438853594746887)





