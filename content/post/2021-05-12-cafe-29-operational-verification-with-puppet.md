---
Title: "29. #EveryoneCanContribute​ cafe: Operational Verification with Puppet"
Date: 2021-05-12
Aliases: []
Tags: ["puppet","testing","monitoring"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---



### Recording

Enjoy the session! 🦊 

{{< youtube N0E4IbwyZEs >}}

<br>

### Highlights

David started with defining the terms of validation and verification, and then stepped into examples of idempotent deployments with Puppet, and how to verify that the deployed catalog actually works. Additional health checks and detecting wrong port mappings. Monitoring shifting left in Infrastructure as Code?

We discussed the future of acceptance testing, and deployment testing - operational verification.

> Special: 1 year #EveryoneCanContribute cafe: https://twitter.com/tonka_2000/status/1392530105373704194

### Insights

- [opv repository](https://github.com/puppetlabs/opv)
  - [check_http](https://github.com/puppetlabs/opv/blob/main/lib/puppet/provider/check_http/check_http.rb)
- [Monitoring == Testing blog](https://puppet.com/blog/hitchhikers-guide-to-testing-infrastructure-as-and-code)
- [Verification and Validation](https://www.easterbrook.ca/steve/2010/11/the-difference-between-verification-and-validation)
- [Twitter thread](https://twitter.com/dnsmichi/status/1392511069030060033)
- [Slides with notes](https://drive.google.com/file/d/1_ru1qAmToHjzXSvzSweT8vdiGTqflg6L/view)



