---
Title: "32. #EveryoneCanContribute cafe: Continuous Profiling with Polar Signals"
Date: 2021-06-02
Aliases: []
Tags: ["monitoring","profiling","observability"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Continuous Profiling - Measure and understand performance over time.

### Recording

Enjoy the session with [Frederic Branczyk](https://twitter.com/fredbrancz) and [Matthias Loibl](https://twitter.com/metalmatze)! 🦊  

{{< youtube IYOleCXROBg >}}

<br>

### Highlights

- Continuous Profiling defining 4 pillars of Observability: Metrics, Traces, Logs, Profiles 
- Open standard: [pprof](https://github.com/google/pprof)
- Polar Signals use Prometheus Service Discovery capabilities. Or push the data every X seconds. 
- Query language similar to PromQL. 
- [Share views as a public URL](https://twitter.com/dnsmichi/status/1400125472315527173), e.g. in PR/MRs for team collaboration. 
- [Merge queries and reports to see which code change impacted the performance](https://twitter.com/dnsmichi/status/1400126364779438086)
- Push metric samples to Thanos, and keep pprof profiles. How to present this over time? (to be solved)
- Idea from Andrew: OpenTelemetry? 
- Automated profiling as an idea, follow [Polar Signals on Twitter](https://twitter.com/PolarSignalsIO) for updates. 
- Optimize for debug symbols - try to only upload once, and link them. 
- Recommendation engine: "Save time with adding this code change".

### Insights

- [Polar signals Website](https://www.polarsignals.com/)
  - [Beta access request](https://www.polarsignals.com/#request-access)
- [perfessor - Continuous Profiling Sidecar](https://github.com/conprof/perfessor)
- [Conprof organization](https://github.com/conprof)
- [Twitter thread](https://twitter.com/dnsmichi/status/1400121372341321734)





