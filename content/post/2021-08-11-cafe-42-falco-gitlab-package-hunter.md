---
Title: "42. #EveryoneCanContribute cafe: Falco and GitLab Package Hunter"
Date: 2021-08-11
Aliases: []
Tags: ["security","falco","cloudnative","gitlab","package-hunter"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[POP](https://twitter.com/danpopnyc) dives into [Falco](https://falco.org/), the rules engine and a live deployment into a Kubernetes cluster. [Michael Friedrich](https://twitter.com/dnsmichi) takes over with Package Hunter, using the Falco rules to monitor unexpected syscalls from package dependency installs. 

### Recording

Enjoy the session! 🦊  

{{< youtube y_M5DjuAbhU >}}

<br>

### Highlights

POP started with the basics, showing the power of the ruleset of Falco to monitor many different events. He also shared how to customize Falco with [Falcosidekicks](https://github.com/falcosecurity/falcosidekick):

> A simple daemon for connecting Falco to your ecossytem. It takes a Falco's events and forward them to different outputs in a fan-out way.

POP showed the sidekicks in the demo with sending messages to Slack, trying to detect a tempering cookie. The Falcosidekick UI shows the events happening, and emergencies detected by the defined rules. You can create a lot of them - and so did GitLab's Application Security team when creating [Package Hunter](https://about.gitlab.com/blog/2021/07/23/announcing-package-hunter/). 

Michael started the Vagrant VM locally, and we tried to send a 2GB tarball into Package hunter, which did not work. After modifying a hardcoded limit, the file could not be parsed - because the limits of the VM. Seeing a potential limit, we'll continue next week with more cloud resources, and ideas and patches to contribute upstream.

The [Falco rules in Package Hunter](https://gitlab.com/gitlab-org/security-products/package-hunter/-/blob/main/falco/falco_rules.local.yaml) adopt Falco's functionality to monitor specific actions such as

- Blacklisted binaries executed in container
- Npm config file access by different programs
- Process starts listening on a port
- Inbound and outbound connections, with an allowed white list of IP addresses and domains

More ideas for the future:

- Falco rulesets contributed upstream
- Move the [Vagrantfile](https://gitlab.com/gitlab-org/security-products/package-hunter/-/blob/main/Vagrantfile) provisioning into Terraform to provisioning a cloud VM
- Add Package Hunter patches for hardcoded values, and error handling (cli.js HOST, routes.js 200mb limit)

After the session, we learned how to enable the full debug mode.

```
$ NODE_ENV=development DEBUG=* node src/server.js
```

More soon, meanwhile checkout Falco and explore its functionality in your production environment! :-) 

### Insights

- Falco
  - [Project](https://falco.org/)
  - [Falcosidekick](https://github.com/falcosecurity/falcosidekick)
  - [awesome-falco - a curated list of Falco related tools, frameworks and articles](https://github.com/developer-guy/awesome-falco)
  - [Contribute to Falco](https://falco.org/docs/contribute/)
  - [Development SDKs](https://github.com/falcosecurity/falco#developing)
  - [Falco as app in GitLab's cluster management template](https://docs.gitlab.com/ee/user/clusters/management_project_template.html#built-in-applications)
  - [Cloud Native Security Hub - Discover and share Kubernetes security best practices and configurations](https://securityhub.dev/)
- Package Hunter
  - [Blog](https://about.gitlab.com/blog/2021/07/23/announcing-package-hunter/)
  - [Server docs](https://gitlab.com/gitlab-org/security-products/package-hunter/)
  - [CLI docs](https://gitlab.com/gitlab-org/security-products/package-hunter-cli)
  - [GitLab Application Security team runbooks: Package Hunter Findings](https://about.gitlab.com/handbook/engineering/security/security-engineering-and-research/application-security/runbooks/investigating-package-hunter-findings.html)
  - [Draft demo project](https://gitlab.com/everyonecancontribute/security/mal-yarn)
- [Twitter thread](https://twitter.com/dnsmichi/status/1422247092362399745)

