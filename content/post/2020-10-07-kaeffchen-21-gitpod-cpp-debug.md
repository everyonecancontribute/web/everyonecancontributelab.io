---
Title: "21. Kaeffchen: GitLab with Gitpod, debugging C++ applications with GDB"
Date: 2020-10-07
Aliases: []
Tags: ["ide","gitpod","learning"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


### Highlights

After exploring [Gitpod last week](https://everyonecancontribute.cafe/post/2020-09-30-kaeffchen-20-gitpod/), we decided to try it out with a C++ project. Actually, we created an error situation to try out the GDB debugger. 

![Gitpod IDE, GitLab and C++ with GDB](/post/images/gitpod_gitlab_cpp_debugger.png)

Yes, a segfault. After investigating with the terminal based gdb command, we thought about how this integrates into Gitpod itself. And voilà, with the following [documentation](https://www.gitpod.io/docs/languages/cpp/#debugging) we were able to integrate GDB on the compiled binary.

```
{
  // Use IntelliSense to learn about possible attributes.
  // Hover to view descriptions of existing attributes.
  "version": "0.2.0",
  "configurations": [
      {
          "type": "gdb",
          "request": "launch",
          "name": "Debug HeyConsole (GDB)",
          "target": "./build/bin/heyconsole",
          "cwd": "${workspaceRoot}",
          "valuesFormatting": "parseText"
      }
  ]
}
```

In order to compile the repository, you need to do the following in the Gitpod terminal:

```
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Debug
make
```


### Recording

Enjoy the session!

{{< youtube Isx16HV6IDs >}}


### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/45)
- Guests: [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [Michael Aigner](https://twitter.com/solidnerd), [Feu Mourek](https://twitter.com/the_feufeu) 
- Next ☕ chat `#21`: **2020-10-08** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/49) 

### Bookmarks

- [Gitpod C++ Debugging](https://www.gitpod.io/docs/languages/cpp/)
- GitLab and GitPod: [Intro video by Marcel van Remmereden](https://www.loom.com/share/9c9711d4876a40869b9294eecb24c54d)
- [Enter the Vault: Authentication Issues in HashiCorp Vault](https://googleprojectzero.blogspot.com/2020/10/enter-the-vault-auth-issues-hashicorp-vault.html)
- [Site Reliability Engineering and how it refers to the DevOps approach](https://medium.com/just-another-buzzword/site-reliability-engineering-and-how-it-refers-to-the-devops-approach-5f641b103de7)
- [The Production Readiness Spectrum](https://dastergon.gr/posts/2020/09/the-production-readiness-spectrum/)
- [Book recommendations](https://twitter.com/dnsmichi/status/1313540191295098882)
  - [Implementing Service Level Objects](https://www.oreilly.com/library/view/implementing-service-level/9781492076803/)
  - [Distributed Tracing in Practice](https://www.oreilly.com/library/view/distributed-tracing-in/9781492056621/)
  - [Monolith to Microservices](https://www.oreilly.com/library/view/monolith-to-microservices/9781492047834/)
  - [Help your kids with computer science](https://www.dk.com/us/book/9780241302293-help-your-kids-with-computer-science/) 






