---
Title: "53. #EveryoneCanContribute Cafe: WebAssembly with AssemblyScript, first try in Gitpod"
Date: 2022-07-12
Aliases: []
Tags: ["webassembly","wasm","typescript","learn"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


{{< youtube H1O2j4w78j8 >}}

<br>

### Insights

[Michael F.](https://twitter.com/dnsmichi), [Michael A.](https://twitter.com/tonka_2000) and [Ryan Perry](https://twitter.com/rperry_) discussed getting started with WebAssembly. After researching a bit, we started a new project based on the [AssemblyScript introduction](https://www.assemblyscript.org/introduction.html#from-a-webassembly-perspective), wondering how things are installed with npm and WebAssembly. After a while, we figured to follow the [AssemblyScript: Setting up a new project guide](https://www.assemblyscript.org/getting-started.html#setting-up-a-new-project) which also populates the `package.json` scripts section.

After we modified the `index.html` body with the `add()` demo example, and successfully ran the website preview inside GitPod, we continue to explore. We then added some div id's to manipulate, keeping the effort as simple as possible and continued adding the functions: `fib()` for integer operations, `hello_from()` for strings, `sort()` and `format_arr()` for sorting arrays, and taught each other both, TypeScript and WebAssembly. 

The whole experience was developed in the browser using [Gitpod](https://www.gitpod.io/); with a file tree, terminal and port 3000 preview. After the meetup, we have added a `.gitpod.yml` configuration file into the [project](), so that everyone can contribute and learn where we left off :-) 

### Resources

- [Learn Wasm project](https://gitlab.com/everyonecancontribute/dev/learn-wasm-assemblyscript) including the Git history from the meetup. 
  - Includes a Gitpod configuration to launch the meetup results for you to learn. 
- [AssemblyScript](https://www.assemblyscript.org/)
  - [Concepts](https://www.assemblyscript.org/concepts.html) and [WebAssembly System Interface (WASI)](https://wasi.dev/)
   - AssemblyScript supports a limited set of types; in order to make types other than numbers (i32, etc.) work across module boundaries, the compilation commands require the `--exportRuntime` flag. [exports.__new is not a function issue](https://github.com/AssemblyScript/assemblyscript/issues/2244).
- [Wasm Cooking with Golang](https://k33g.gumroad.com/l/wasmcooking) book by Philippe Charrière
- [WebAssembly in the opsindev.news newsletter July 2022](https://opsindev.news/archive/2022-07-12/#your-next-project-could-be)

### Future ideas

1. Help improve the developer experience. The learning curve is very steep. 
1. Build WebAssembly in GitLab CI/CD
1. Upload the wasm files into the [generic package registry](https://docs.gitlab.com/ee/user/packages/generic_packages/) in GitLab, and consume the wasm files externally.
1. Security scanning 
1. [WASI tutorial](https://github.com/bytecodealliance/wasmtime/blob/main/docs/WASI-tutorial.md#running-common-languages-with-wasi)

### News

The next meetup happens on [Augst 9, 2022](https://www.meetup.com/everyonecancontribute-cafe/events/286609523/). 

We will meet on the second Tuesday at 9am PT. 





