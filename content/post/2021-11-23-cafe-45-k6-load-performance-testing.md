---
Title: "45. #EveryoneCanContribute cafe: Load Performance Testing with k6"
Date: 2021-11-23
Aliases: []
Tags: ["testing","performance","k6","slo","prometheus","grafana","monitoring"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Nicole van der Hoeven](https://twitter.com/n_vanderhoeven) and [Floor Drees](https://twitter.com/FloorDrees) from [k6](https://twitter.com/k6_io) provided a demo deep-dive, going straight into Pikachu load testing. From extending and integrating k6, we heard more feature announcements hot off the press after [Grafana's ObservabilityCon](https://grafana.com/about/events/observabilitycon/2021/). 

{{< youtube _ty40gSaaw8 >}}

<br>

### Insights

We've learned about

- k6 and its [extensions ecosystem](https://k6.io/docs/extensions/): Add Chaos to your Kubernetes cluster
- [k6 cloud as a datasource for Grafana](https://twitter.com/dnsmichi/status/1463200201733287937)
- [Prometheus remote writes into Grafana Cloud](https://twitter.com/dnsmichi/status/1463201263080939523) or any other Prometheus instance with [k6 extensions](https://github.com/grafana/xk6-client-prometheus-remote)
- [Web browser testing](https://twitter.com/dnsmichi/status/1463203064232521738)
- [k6 Kubernetes Operator](https://twitter.com/dnsmichi/status/1463203909581914114)
- [k6 Office Hours: How GitLab uses k6 with Grant Young](https://www.youtube.com/watch?v=YTGkq0m1bYk)

Ideas

- Let k6 use Prometheus remote writes to store metrics into an [Opstrace](/post/2021-04-14-cafe-25-opstrace-observability/) cluster, and integrate the cluster with [Keptn](/post/2020-11-11-cafe-8-keptn/) as quality gate for GitLab CI/CD.
- k6 for performance testing, distributed tracing in the backend. Combine the tracing spec between k6 and [Grafana Tempo](/post/2020-10-28-cafe-6-grafana-tempo/)?

### News

After taking a break and [discussing the future of the #EveryoneCanContribute cafe](https://gitlab.com/groups/everyonecancontribute/-/epics/2), we created a new [meetup group](https://www.meetup.com/everyonecancontribute-cafe/), starting with this cafe. We'll also switch to a monthly cadence, keeping social hours in [Discord](/page/join). Future cafe meetups will be announced in the [group](/page/handbook/#groups), with easier access to joining Zoom :) 


