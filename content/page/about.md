---
Title: "About"
Date: 2020-12-07
Authors: ["dnsmichi"]
mermaid: true
---

Technology is moving fast, and often we want to try things but lack the time to. Best practices and learnings need discussion, or a live debugging session in a cosy coffee chat.

* [Join us](/page/join/)
* [Who we are](#who-we-are)
* [Event Calendar](/page/events/)

This idea formed the `#EveryoneCanContribute` cafe, 1 hour social chat with a topic or theme. From "What did you learn recently" to project showcases and learning (debugging) together. Often community members and thought leaders join and share insights and discuss fresh ideas in the group.

Events are organized in our [meetup group](/page/join/) and are shown in the [event calendar](/page/events/). The meetups are hosted by [Michael Friedrich, Senior Developer Evangelist at GitLab](/post/2020-05-28-first-post/). Check our [Handbook](/page/handbook/) for more insights.

## We already learned 

### Cloud Native

- [Falco and Package Hunter](/post/2021-08-11-cafe-42-falco-gitlab-package-hunter/) with [POP](https://twitter.com/danpopnyc)
- [Litmus - Chaos Engineering for your Kubernetes](/post/2021-06-23-cafe-35-litmus-chaos-engineering-kubernetes/) with [Prithvi Rai](https://twitter.com/prithvi137) and [Karthik Satchitanand](https://twitter.com/ksatchit)
- [Civo Cloud, k3s and GitLab](/post/2021-07-21-cafe-39-civo-cloud-k3s-gitlab/) with [Anaïs Urlichs](https://twitter.com/urlichsanais)
- [Keptn, a control pane for CD in cloud-native apps](/post/2020-11-11-cafe-8-keptn/) with [Alois Reitbauer](https://twitter.com/aloisreitbauer) and [Christian Heckelmann](https://twitter.com/wurstsalat)

Kubernetes learning series:

- Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) with [Max](https://twitter.com/ekeih).
- Deployed [k3s as Kubernetes distribution](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) with [Max](https://twitter.com/ekeih).
- Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/) with [Max](https://twitter.com/ekeih).
- [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/) with [Max](https://twitter.com/ekeih).
- [Get to know Kubernetes user authentication and authorization with RBAC](/post/2021-02-24-cafe-18-kubernetes-rbac-authentication-authorization/) with [Niclas Mietz](https://twitter.com/solidnerd).
- [Break into Kubernetes Security](/post/2021-03-03-cafe-19-break-into-kubernetes-security/) with [Philip Welz](https://twitter.com/philip_welz).
- [Securing Kubernetes with Kyverno](/post/2021-03-10-cafe-20-securing-kubernetes-with-kyverno/) with [Philip Welz](https://twitter.com/philip_welz).
- [Secure Kubernetes with OpenID](/post/2021-03-17-cafe-21-kubernetes-security-openid-kiosk/) with [Niclas Mietz](https://twitter.com/solidnerd).
- [Multi-tenancy with Kiosk in Kubernetes](/post/2021-03-24-cafe-22-multi-tenancy-with-kiosk-in-kubernetes/) with [Niclas Mietz](https://twitter.com/solidnerd).
- [Automate our Kubernetes setup & deep dive into Hetzner firewall](/post/2021-03-31-cafe-23-automate-kubernetes-setup-hetzner-firewall-feature/) with [Max](https://twitter.com/ekeih).
- [Automate Kubernetes deployment with Terraform and GitLab CI/CD](/post/2021-04-07-cafe-24-automate-kubernetes-deployment-ansible-gitlab-cicd/)
- [Automate Kubernetes deployment with Terraform and GitLab CI/CD, iteration 2](/post/2021-04-21-cafe-26-automate-kubernetes-deployment-gitlab-ci-cd-iteration-2/)
- [GitLab Kubernetes Agent and GitOps](/post/2021-04-28-cafe-27-gitlab-kubernetes-agent-gitops/)
- [Kubernetes Monitoring with Prometheus](/post/2021-05-19-cafe-30-kubernetes-monitoring-prometheus/)
- [Policy reporter for Kyverno](/post/2021-07-07-cafe-37-policy-reporter-for-kyverno/) with [Frank Jogeleit](https://twitter.com/FrankJogeleit) 
- [Talos, a Kubernetes OS](/post/2021-07-14-cafe-38-talos-kubernetes-os/) with [Andrew Rynhard](https://twitter.com/andrewrynhard)

### Dev

- [First steps with Rust in Gitpod](/post/2020-10-07-cafe-3-gitpod-gitlab-rust/)
- [Learn Rust with Rocket web app & Prometheus Monitoring](/post/2021-06-30-cafe-36-rust-rocket-prometheus/)
- [Machine Learning for Software Developers (...and Knitters)](/post/2021-05-26-cafe-31-machine-learning/) with [Kris Howard](https://twitter.com/web_goddess) 
- [Jina.ai, an OSS neural search framework](/post/2020-10-14-cafe-4-jina-ai/) with [Alex Cureton-Griffiths](https://twitter.com/alexcg), [Pei-Shan Wu](https://twitter.com/pswu11), [Rutujua Surve](https://www.linkedin.com/in/rutuja-surve-150b4080/), [Pratik Bhavsar](https://www.linkedin.com/in/bhavsarpratik/)

### Sec

- [CI/CD Security with Hashicorp Vault](/post/2020-09-30-cafe-2-vault-ci/) with [Niclas Mietz](https://twitter.com/solidnerd)
- [Cloud native security with Snyk](/post/2021-06-09-cafe-33-cloud-native-security-snyk/) with [Matt Jarvis](https://twitter.com/mattj_io)
- [Kubernetes Cluster Image Scanning with Trivy & Starboard](/post/2021-08-04-cafe-41-kubernetes-cluster-image-scanning-trivy-starboard/)

### Ops

- [QuestDB](/post/2020-09-23-cafe-1/) with [Vlad Ilyushchenko](https://twitter.com/ilyusvl), [Nicolas Hourcard](https://twitter.com/nicolas_hrd), [David G. Simmons](https://twitter.com/davidgsIoT)
- [Grafana Tempo](/post/2020-10-28-cafe-6-grafana-tempo/)
- [HashiCorp Waypoint](/post/2020-10-21-cafe-5-hashicorp-waypoint/)
- [Observability with Opstrace](/post/2021-04-14-cafe-25-opstrace-observability/) with [Sebastien Pahl](https://twitter.com/sebp) and [Mat Appelman](https://twitter.com/matappelman)
- [Continuous Profiling with Polar Signals](/post/2021-06-02-cafe-32-polar-signals-continuous-profiling/) with [Frederic Branczyk](https://twitter.com/fredbrancz) and [Matthias Loibl](https://twitter.com/metalmatze)
- [Terraform and Helm Registries in GitLab](/post/2021-07-28-cafe-40-terraform-helm-gitlab-registry/)
- [Docker Hub rate limit mitigation](/post/2020-11-04-cafe-7-docker-hub-rate-limit-monitoring/)

<br>

## Who we are

### Core Team

Continuous attendance and contributed/hosted 1+ sessions to learn together.

| Name               | <i class="fab fa-twitter"></i> Twitter                  | <i class="fab fa-linkedin"></i> LinkedIn                                        | <i class="fas fa-draw-polygon"></i> Polywork  | <i class="fab fa-gitlab"></i> GitLab                  |
| ------------------ | ------------------------------------------------------- | ------------------------------------------------------------------------------- | --------------------------------------------- | ----------------------------------------------------- |
| Michael Friedrich  | [@dnsmichi](https://twitter.com/dnsmichi)               | [Michael Friedrich](https://www.linkedin.com/in/dnsmichi/)                      | [dnsmichi](https://polywork.com/dnsmichi)     | [dnsmichi](https://gitlab.com)                        |
| Nico Meisenzahl    | [@nmeisenzahl](https://twitter.com/nmeisenzahl)         | [Nico Meisenzahl](https://www.linkedin.com/in/nicomeisenzahl/)                  | -                                             | [nico-meisenzahl](https://gitlab.com/nico-meisenzahl) |
| Niclas Mietz       | [@solidnerd](https://twitter.com/solidnerd)             | [Niclas Mietz](https://www.linkedin.com/in/niclas-mietz-794053115/)             | [solidnerd](https://polywork.com/solidnerd)   | [solidnerd](https://gitlab.com/solidnerd)             |
| Philip Welz        | [@philip_welz](https://twitter.com/philip_welz)         | [Philip Welz](https://www.linkedin.com/in/philip-welz/)                         | [pwelz](https://polywork.com/pwelz)           | [phil.xx](https://gitlab.com/phil.xx)                 |
| Michael Aigner     | [@tonka3000](https://twitter.com/@tonka_2000)           | [Michael Aigner](https://www.linkedin.com/in/michael-aigner-ab06809/)           | [tonka3000](https://polywork.com/tonka3000)   | [tonka3000](https://gitlab.com/tonka3000)             |
| David Schmitt      | [@dev_el_ops](https://twitter.com/dev_el_ops)           | [David Schmitt](https://www.linkedin.com/in/davidschmitt/)                      | [dev_el_ops](https://polywork.com/dev_el_ops) | [DavidS](https://gitlab.com/DavidS)                   |
| Max Rosin          | [@ekeih](https://twitter.com/ekeih)           | -                      | - | [ekeih](https://gitlab.com/ekeih)                   |

<br>

### Contributors

| Name               | <i class="fab fa-twitter"></i> Twitter                  | <i class="fab fa-linkedin"></i> LinkedIn                                        | <i class="fas fa-draw-polygon"></i> Polywork  | <i class="fab fa-gitlab"></i> GitLab                  |
| ------------------ | ------------------------------------------------------- | ------------------------------------------------------------------------------- | --------------------------------------------- | ----------------------------------------------------- |
| Sven Patrick Meier    | [@_spse_](https://twitter.com/_spse)                  | [Sven Patrick Meier](https://www.linkedin.com/in/sven-patrick-meier-213a881a8/)       | -                                             |  [SvenPatrick](https://gitlab.com/SvenPatrick)                    |
| Marcel Weinberg    | [@winem_](https://twitter.com/@winem_)                  | [Marcel Weinberg](https://www.linkedin.com/in/marcel-weinberg-a86464101/)       | -                                             | [winem](https://gitlab.com/winem)                     |
| Feu Mourek         | [@the_FeuFeu](https://twitter.com/the_feufeu)           | [Feu Mourek](https://www.linkedin.com/in/feu-mourek/)                           | -                                             | [theFeu](https://gitlab.com/theFeu)                   |
| Philipp Westphalen | [@Koala_Phil](https://twitter.com/Koala_Phil)           | [Philipp Westphalen](https://www.linkedin.com/in/philipp-westphalen-a83318188/) | -                                             | [Phil404](https://gitlab.com/Phil404)                 |
| Nicolai Buchwitz   | [@nicolaibuchwitz](https://twitter.com/nicolaibuchwitz) | [Nicolai Buchwitz](https://www.linkedin.com/in/nicolai-buchwitz-06174680/)      | -                                             | [nbuchwitz](https://gitlab.com/nbuchwitz)             |
| Mario Kleinsasser  | [@m4r10k](https://twitter.com/m4r10k)                   | [Mario Kleinsasser](https://www.linkedin.com/in/mario-kleinsasser/)             | -                                             | [m4r10k](https://gitlab.com/m4r10k)                   |
| Bernhard Rausch    | [@rauschbit](https://twitter.com/rauschbit)             | [Bernhard Rausch](https://www.linkedin.com/in/bernhard-rausch/)                 | -                                             | [rauschbit](https://gitlab.com/rauschbit)             |
| Markus Fischbacher | [@rockaut](https://twitter.com/@RealRockaut)            | -                                                                               | -                                             | [rockaut](https://gitlab.com/rockaut)                 |
| Carsten Köbke      | [@mikeschova](https://twitter.com/mikeschova)           | [Carsten Köbke](https://www.linkedin.com/in/carsten-koebke-a721ab161/)          | -                                             | [Mikeschova](https://gitlab.com/Mikeschova)           |
| Moritz Tanzer      | [@moritztanzer](https://twitter.com/moritztanzer)       | [Moritz Tanzer](https://www.linkedin.com/in/moritz-tanzer-74292a141/)           | -                                             | [tanzerm](https://gitlab.com/tanzerm)                 |
| Tim Meusel         | [@BastelsBlog](https://twitter.com/BastelsBlog)         | -                                                                               | -                                             | -                                                     |

